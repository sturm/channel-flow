;;; Directory Local Variables            -*- no-byte-compile: t -*-
;;; For more information see (info "(emacs) Directory Variables")

((nil . ((cider-default-cljs-repl . shadow)
         (cider-shadow-default-options . ":app")
         (cider-offer-to-open-cljs-app-in-browser . nil)))
 (web-mode . ((web-mode-markup-indent-offset . 2))))
