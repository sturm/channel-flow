# Channel Flow Calculater

A ClojureScript app to help farmers estimate how much water can flow down an earthen channel without causing soil erosion.

## Development

During development you can automatically rebuild when the source is changed:

    npx shadow-cljs watch app

Visit: http://localhost:9090

Alternatively, for an editor-connected REPL in Emacs, run:

    cider-jack-in-cljs

## Production

To build for production:

    npx shadow-cljs release app
