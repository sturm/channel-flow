# Change Log

## [0.2.0] - 2016-06-06
### Changed
- Add `externs.js` to protect form from compiler name munging

## [0.1.0] - 2016-05-08
Initial release of ClojureScript version.
