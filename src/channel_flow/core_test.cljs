(ns channel-flow.core-test
  (:require
   [channel-flow.core]
   [clojure.test :refer [deftest is]]))

(deftest mannings-test
  (is (== (channel-flow.core/mannings 5 4 2 0.2 0.02 1)
          {:flow-width 4 :velocity 187.16882827270024 :flow 124.77921884846683})))
