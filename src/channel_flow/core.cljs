;; Channel Flow Calculator
;; Copyright (C) 2016 Ben Sturmfels

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(ns channel-flow.core
  (:require goog.dom
            goog.events))

(defn nandash
  [number]
  (if (js/isNaN number)
    "–"
    number))

(defn mannings
  "Calculate Mannings formula."
  [w1 w2 cd s m fd]
  (let [exp js/Math.pow
        sqrt js/Math.sqrt

        ;; Flow width.
        fw (+ w2 (/ (* fd (- w1 w2)) cd))

        ;; Area.
        a (* fd (+ w2 (/ (- fw w2) 2)))

        ;; Wetted perimeter.
        p (+ (* 2 (sqrt (+ (exp (/ (- fw w2) 2) 2.0) (exp fd 2.0)))) w2)

        ;; Hydraulic radius.
        ;; Avoid dividing by zero.
        r (if (zero? p)
            0
            (/ a p))

        ;; Velocity.
        v (/ (* (exp r (/ 2.0 3.0)) (exp s 0.5)) m)

        ;; Flow.
        q (* a v)]
    (println "Flow width:" fw)
    (println "Area:" a)
    (println "Wetted perimeter:" p)
    (println "Hydraulic radius:" r)
    (println "Velocity:" v)
    (println "Flow:" q)
    {:flow-width fw :velocity v :flow q}))

(defn transforms
  "Calculate the transformation factors to utilise full canvas."
  [w1 w2 cd width]
  (let [maxsize (- width 10)]
    ;; find largest of w1, w2 and cd
    (cond
      (and (>= w1 w2) (>= w1 cd)) {:scalefactor (/ maxsize w1)
                                   :transX 0 :transY 0}
      ;; translate by 1/2 of difference between top and bottom
      (and (>= w2 w1) (>= w2 cd)) {:scalefactor (/ maxsize w2)
                                   :transX (/ (- w2 w1) 2) :transY 0}
      :else {:scalefactor (/ maxsize cd)
             :transX (if (< w1 w2) (/ (- w2 w1) 2) 0) :transY 0})))

(defn validate-width
  [w1 w2]
  (if (< w1 w2)
    "<li class='alert alert-danger'>Top width must be greater than or equal to bottom width.</li>"
    ""))

(defn validate-depth
  [cd fd]
  (if (< cd fd)
    "<li class='alert alert-danger'>Flow depth must be less than or equal to channel depth.</li>"
    ""))

(defn validate
  "Validates the inputs."
  [w1 w2 cd fd]
  (str (validate-width w1 w2)
       (validate-depth cd fd)))

(defn clear-diagram
  [canvas]
  (set! canvas.width canvas.width))

(defn draw-diagram
  "Draw the channel."
  [canvas w1 w2 cd _s _m fd fw]

  (let [ctx (.getContext canvas "2d")
        {scalefactor :scalefactor transX :transX transY :transY} (transforms w1 w2 cd canvas.width)]
    ;; Transform the diagram to fit canvas.
    (.scale ctx scalefactor scalefactor)
    (.translate ctx transX transY)

    ;; Draw water.
    (.beginPath ctx)
    (.moveTo ctx (- (/ w1 2) (/ fw 2)) (- cd fd))
    (.lineTo ctx (+ (/ w1 2) (/ fw 2)) (- cd fd))
    (.lineTo ctx (+ (/ w1 2) (/ w2 2)) cd) ;; down left side
    (.lineTo ctx (- (/ w1 2) (/ w2 2)) cd) ;; across bottom
    (let [grd (.createLinearGradient ctx 0  0  w1 cd)]
      ;; light blue
      (.addColorStop grd 0 "#8ED6FF")
      ;; dark blue
      (.addColorStop grd 1 "#004CB3")
      (set! ctx.fillStyle grd))
    (.closePath ctx)
    (.fill ctx)

    ;; Draw channel.
    (.beginPath ctx)
    (.moveTo ctx 0 0)
    (.lineTo ctx (- (/ w1 2) (/ w2 2)) cd) ;; down left side
    (.lineTo ctx (+ (/ w1 2) (/ w2 2)) cd) ;; across bottom
    (.lineTo ctx w1 0) ;; up right side
    (set! ctx.strokeStyle "#633")
    (set! ctx.lineWidth 0.1)
    (.stroke ctx)
    (.closePath ctx)))

(defn recalculate
  []
  (let [w1 (.-valueAsNumber js/document.mannings.w1)
        w2 (.-valueAsNumber js/document.mannings.w2)
        cd (.-valueAsNumber js/document.mannings.cd)
        s (.-valueAsNumber js/document.mannings.s)
        m (.-valueAsNumber js/document.mannings.m)
        fd (.-valueAsNumber js/document.mannings.fd)
        error (goog.dom/getElement "error")
        canvas (goog.dom/getElement "square")
        valid_msg (validate w1 w2 cd fd)]

    ;; Clear the canvas early in case validation fails.
    (clear-diagram canvas)

    (if (= valid_msg "")
      (let [{fw :flow-width v :velocity q :flow} (mannings w1 w2 cd s m fd)]
        (set! error.innerHTML "")
        (set! (.-value js/document.mannings.v) (nandash (.toFixed v 2)))
        (set! (.-value js/document.mannings.q) (nandash (.toFixed q 2)))
        (draw-diagram canvas w1 w2 cd s m fd fw))
      (set! error.innerHTML valid_msg))))

;; Create some static data
(def results (atom {:w1 5}))

(defn onload
  []
  (goog.events/listen js/document.mannings "input" recalculate)
  (recalculate))

(set! js/window.onload onload)
